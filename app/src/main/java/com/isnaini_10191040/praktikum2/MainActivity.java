package com.isnaini_10191040.praktikum2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.isnaini_10191040.praktikum2.activity.FragmentActivity;
import com.isnaini_10191040.praktikum2.activity.LifecycleActivity;

public class MainActivity extends AppCompatActivity {

    ImageView image;
    boolean status;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnLifeCycle=findViewById(R.id.btnLifeCycle);
        Button btnFragment=findViewById(R.id.btnFragment);
        Button btnLamp=findViewById(R.id.btnLamp);
        image=findViewById(R.id.Image);
        status=false;

        btnLifeCycle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MainActivity.this, LifecycleActivity.class);
                startActivity(intent);
            }
        });

        btnFragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MainActivity.this, FragmentActivity.class);
                startActivity(intent);
            }
        });

        btnLamp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onoff();

            }
        });


    }
    public void onoff(){
        if (!status){
            image.setImageResource(R.drawable.on);

        }
        else{
            image.setImageResource(R.drawable.off);
        }
    }
}