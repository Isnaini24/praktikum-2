package com.isnaini_10191040.praktikum2.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.isnaini_10191040.praktikum2.R;
import com.isnaini_10191040.praktikum2.fragment.FirstFragment;
import com.isnaini_10191040.praktikum2.fragment.TwoFragment;

public class FragmentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);

        Button fragment1=findViewById(R.id.fragment1);
        Button fragment2=findViewById(R.id.fragment2);

        fragment1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager FM=getSupportFragmentManager();
                FragmentTransaction FT=FM.beginTransaction();
                FT.replace(R.id.relative, new FirstFragment());

            }
        });

        fragment2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager FM=getSupportFragmentManager();
                FragmentTransaction FT=FM.beginTransaction();
                FT.replace(R.id.relative, new TwoFragment());
            }
        });
    }
}